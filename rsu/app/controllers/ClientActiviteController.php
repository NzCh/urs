<?php
use App\Controllers;
use Core\Http\Request;
//use Core\Http\Response;
use Phalcon\Http\Response;
use App\Models;



class ClientActiviteController extends PrivateController {

/**************************************************************************************************************************************************************************************************/
//                Ajouter une nouvelle activité pour le client physique            |
//POST:http://localhost:8000/rsu/ClientActivite/addActivite (If you want to add new activity of client you need the id_activite of the new activity that you want to add and the id_client)                     |
/**************************************************************************************************************************************************************************************************/

    public function addActiviteAction() {
    
    $request = new Request();
    $errors=[];
    $params=[];

    $params['id_activite'] = $this->request->getPost('id_activite');

    $params['id_client'] = $this->request->getPost('id_client');

    $response_data = ClientActiviteModel::addActiviteClientPhysique($params);
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

    }

/**********************************************************************************/
//                Supprimer une activité d'un client physique                      |
//DELETE:http://localhost:8000/rsu/ClientActivite/deleteActivite/uuid_activite/    |
/**********************************************************************************/
 
    public function deleteActiviteAction() {

        //$request = new Request();
        // $params['id_client'] = $this->request->getPost('id_client');
        // $params['id_activite'] = $this->request->getPost('id_activite');
        $params['uuid_activite'] = $this->dispatcher->getParam('uuid_activite');
        $response_data = ClientActiviteModel::deleteActiviteClientPhysique($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    }


/********************************************************************************************************************/
//                Afficher les activitées d'un client on utilisont Id_client

// GET:http://localhost:8000/rsu/ClientActivite/selectActivitiesById/id_client/3
/********************************************************************************************************************/
public function selectActivitiesByIdAction() {

    //$request = new Request();  
    $id_client = $this->dispatcher->getParam('id_client');
    $response_data = ClientActiviteModel::selectActivitesClientPhysique($id_client);
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

}

/********************************************************************************************************************/
//                Afficher les activitées d'un client on utilisons Uuid Client
//GET : http://localhost:8000/rsu/ClientActivite/selectActivitiesByUuid/uuid_client/5b480698-f265-4517-9670-c338a15fcd26 (give uuid_client to get his activities)
/********************************************************************************************************************/    

public function selectActivitiesByUuidAction() {

    $request = new Request();  
    $uuid_client = $this->request->getPost('uuid_client');
    $response_data = ClientActiviteModel::selectActivitesClientPhysiqueUuid("'".$uuid_client."'");
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

}

/********************************************************************************************************************/
//                Afficher les activitées des clients 

// GET:http://localhost:8000/rsu/ClientActivite/selectASFClients
/********************************************************************************************************************/
public function selectASFClientsAction() {

    $response_data = ClientActiviteModel::getListeAFSClients();
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

}





}    