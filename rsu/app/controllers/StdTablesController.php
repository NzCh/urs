<?php
use App\Controllers;
use Core\Http\Request;
//use Core\Http\Response;
use Phalcon\Http\Response;
use App\Models;


class StdTablesController extends PrivateController{
   
//------------- Récupérer tous les éléments d'une table de nomenclature -------------//  
    //Villes
    //GET: http://localhost:8000/rsu/StdTables/listeVilles
    
    public function listeVillesAction() {
 
        $response_data = StdTablesModel::getListeVilles();
        $result['villes'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;

    }

    //POST: http://localhost:8000/rsu/StdTables/addVille
    public function addVilleAction() {
        
        $request = new Request();
        $params=[];

        $params['nom_ville'] = $this->request->getPost('nom_ville');
        $response_data = StdTablesModel::addVille($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    }

    //----------------Secteurs-----------------------//
    //http://localhost:8000/rsu/StdTables/listeSecteurs
    public function listeSecteursAction() {
 
        $response_data = StdTablesModel::getListeSecteurs();
        $result['Secteurs'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //add secteur
    //POST: http://localhost:8000/rsu/StdTables/addSecteur
    public function addSecteurAction() {
        
        $request = new Request();
        $params=[];
        $params['secteur'] = $this->request->getPost('secteur');
        $response_data = StdTablesModel::addSecteur($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;
    
    }
    
    //----------------Fileres--------------------------//
    //http://localhost:8000/rsu/StdTables/listeFilieres

    public function listeFilieresAction() {
 
        $response_data = StdTablesModel::getListeFilieres();
        $result['Filieres'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    // //POST: http://localhost:8000/rsu/StdTables/addFiliere
    public function addFiliereAction() {
        
        $request = new Request();
        $params=[];
        
        $params['filiere'] = $this->request->getPost('filiere');
        $params['id_secteur'] = $this->request->getPost('id_secteur');
        $response_data = StdTablesModel::addFiliere($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;
    
    }    


     //Activités
     //http://localhost:8000/rsu/StdTables/listeActivites
     public function listeActivitesAction() {
 
        $response_data = StdTablesModel::getListeActivites();
        $result['Activites'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;

    }

 
//POST: http://localhost:8000/rsu/StdTables/addActivite
public function addActiviteAction() {
        
    $request = new Request();
    $params=[];
    
    $params['activite'] = $this->request->getPost('activite');
    $params['id_filiere'] = $this->request->getPost('id_filiere');
    $response_data = StdTablesModel::addActivite($params);        
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

} 
        

    //Communes
    //POST: http://localhost:8000/rsu/StdTables/addCommune
    public function addCommuneAction() {
        
        $request = new Request();
        $errors=[];
        $params=[];
        
        $params['commune'] = $this->request->getPost('commune');
        $response_data = StdTablesModel::addCommune($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

   } 
       
    //http://localhost:8000/rsu/StdTables/listeCommunes

    public function listeCommunesAction() {
 
        $response_data = StdTablesModel::getListeCommunes();
        $result['Communes'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;

    }

    //Provinces
    //http://localhost:8000/rsu/StdTables/listeProvinces

    public function listeProvincesAction() {
 
        $response_data = StdTablesModel::getListeProvinces();
        $result['Provinces'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //Pays
     //http://localhost:8000/rsu/StdTables/listePays

    public function listePaysAction() {
 
        $response_data = StdTablesModel::getListePays();
        $result['Pays'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //POST: http://localhost:8000/rsu/StdTables/addpays
    public function addPaysAction() {
        
        $request = new Request();
        $params=[];
            
        $params['libelle_pays'] = $this->request->getPost('libelle_pays');
        $response_data = StdTablesModel::addPays($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;
    
    } 

    //Qualites
    //http://localhost:8000/rsu/StdTables/listeQualites
    
    public function listeQualitesAction() {
 
        $response_data = StdTablesModel::getListeQualites();
        $result['Qualites'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //POST: http://localhost:8000/rsu/StdTables/addqualite
    public function addQualiteAction() {
        
        $request = new Request();
        $params=[];

        $params['qualite'] = $this->request->getPost('qualite');
        $response_data = StdTablesModel::addQualite($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    } 

    //Voies
    //http://localhost:8000/rsu/StdTables/listeVoies

       public function listeVoiesAction() {
 
        $response_data = StdTablesModel::getListeVoies();
        $result['Voies'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //POST: http://localhost:8000/rsu/StdTables/addvoie
    public function addVoieAction() {
        
        $request = new Request();
        $params=[];
        
        $params['libelle_voie'] = $this->request->getPost('libelle_voie');
        $response_data = StdTablesModel::addVoie($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    } 

      //Organismes d'Affiliation
    //http://localhost:8000/rsu/StdTables/listeOrganismesAffiliation

    public function listeOrganismesAffiliationAction() {
 
        $response_data = StdTablesModel::getListeOrganismesAffiliation();
        $result['Organismes'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //POST: http://localhost:8000/rsu/StdTables/addorganismeaffiliation
    public function addOrganismeAffiliationAction() {
        
        $request = new Request();
        $params=[];
            
        $params['nom_organisme_affiliation'] = $this->request->getPost('nom_organisme_affiliation');
        $response_data = StdTablesModel::addOrganismeAffiliation($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    } 

     //Categorie VIP
    //http://localhost:8000/rsu/StdTables/listeCategorieVip

     public function listeCategorieVipAction() {
 
        $response_data = StdTablesModel::getListeCategorieVip();
        $result['CategorieVip'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //POST: http://localhost:8000/rsu/StdTables/addcategorievip
    public function addCategorieVipAction() {
        
        $request = new Request();
        $params=[];
            
        $params['categorie_vip'] = $this->request->getPost('categorie_vip');
        $response_data = StdTablesModel::addCategorieVip($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;
        
    } 

     //Professions
    //http://localhost:8000/rsu/StdTables/listeProfessions

     public function listeProfessionsAction() {
 
        $response_data = StdTablesModel::getListeProfessions();
        $result['Professions'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }
        
    //POST: http://localhost:8000/rsu/StdTables/addprofession
    public function addProfessionAction() {
        
        $request = new Request();
        $params=[];
            
        $params['profession'] = $this->request->getPost('profession');
        $response_data = StdTablesModel::addProfession($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;
        
    } 

    //Formes Juridiques
    //http://localhost:8000/rsu/StdTables/listeFormesJuridiques

    public function listeFormesJuridiquesAction() {
 
        $response_data = StdTablesModel::getListeFormesJuridiques();
        $result['FormesJuridiques'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //POST: http://localhost:8000/rsu/StdTables/addformejuridique
    public function addFormeJuridiqueAction() {
        
        $request = new Request();
        $params=[];
            
        $params['forme_juridique'] = $this->request->getPost('forme_juridique');
        $response_data = StdTablesModel::addFormeJuridique($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;
        
    }     
    

    //VIP
    //http://localhost:8000/rsu/StdTables/listeVip

    public function listeVipAction() {
 
            $response_data = StdTablesModel::getListeVip();
            $result['VIP'] = $response_data;
            $response = new Response();
            $response->setContent(json_encode($response_data));   
            return $response;
            
    }

    //Type Document
    //http://localhost:8000/rsu/StdTables/listeTypeDocument

    public function listeTypeDocumentAction() {
 
        $response_data = StdTablesModel::getListeTypeDocument();
        $result['Type Document'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;
        
    }

    //Activité secteur filiere
    //http://localhost:8000/rsu/StdTables/listeActivitesFilieresSecteurs
    public function listeActivitesFilieresSecteursAction(){

        $response_data = StdTablesModel::getListeAFS();
        $result['AFS'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;

    }

    //Sexe client
    //http://localhost:8000/rsu/StdTables/listeSexeClient
    public function listeSexeClientAction(){

        $response_data = StdTablesModel::getListeSexeClient();
        $result['Gender'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;

    }

    //ajouter Activite filiere secteur
    //POST : http://localhost:8000/rsu/StdTables/addASF

    public function addASFAction() {
        
        $request = new Request();
        $params=[];
        $params['secteur'] = $this->request->getPost('secteur');
        $params['filiere'] = $this->request->getPost('filiere');
        $params['activite'] = $this->request->getPost('activite');

        $response_data = StdTablesModel::addActiviteSecteurFiliere($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response ;
    
    }




}    

