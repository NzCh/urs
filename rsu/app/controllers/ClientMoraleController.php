<?php

use App\Controllers;
use Core\Http\Request;
//use Core\Http\Response;
use Phalcon\Http\Response;
use App\Models;

class ClientMoraleController extends PrivateController {


/********************************************************************************************************************/
//       Ajouter client morale 
//Post : http://localhost:8000/rsu/ClientMorale/addClientMorale
/********************************************************************************************************************/   

    public function addClientMoraleAction() {
        $request = new Request();
        $errors=[];
        $params=[];

        $params['raison_sociale'] = $this->request->getPost('raison_sociale');
        if ((empty($params['raison_sociale'])) || (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['raison_sociale']))) {
            $errors['raison_sociale'] = 'la Raison Social est obligatoire et doit consiste des lettres en majuscule';
        }

        $params['nom_abreg'] = $this->request->getPost('nom_abreg');
        if ((empty($params['nom_abreg'])) || (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['nom_abreg']))) {
            $errors['nom_abreg '] = 'Erreur Nom Abrégé doit consiste des lettres en majuscule';
        }

        $params['id_forme_juridique'] = $this->request->getPost('id_forme_juridique');

        $params['tel_fixe'] = $this->request->getPost('tel_fixe');
        if ((empty($params['tel_fixe'])) || (!preg_match('/^[0-9 ]{2,30}$/', $params['tel_fixe']))) {
            $errors['tel_fixe'] = 'Erreur le telephone fixe doit contient seulement des chiffres';
        }

        $params['fax'] = $this->request->getPost('fax');
        if ((empty($params['fax'])) || (!preg_match('/^[0-9 ]{2,30}$/', $params['fax']))) {
            $errors['fax'] = 'Erreur FAX doit contient seulement des chiffres';
        }

        $params['siteweb'] = $this->request->getPost('siteweb',null,' ');

        $params['nom_holding'] = $this->request->getPost('nom_holding');
        if ((empty($params['nom_holding'])) || (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['nom_holding']))){
            $errors['nom_holding'] = 'Erreur Nom du holding doit contient des alphabets en majuscule';
        }

        $params['numero_patente'] = $this->request->getPost('numero_patente');
        if ((empty($params['numero_patente'])) || (!preg_match('/^[0-9 ]{1,15}$/', $params['numero_patente']))) {
            $errors['numero_patente'] = 'Erreur le Numero de patente doit contient seulement des chiffres';
        }
        
        $params['identifient_fiscale'] = $this->request->getPost('identifient_fiscale');
        if((empty($params['identifient_fiscale'])) || (!preg_match('/[0-9 ]{2,100}/', $params['identifient_fiscale']))){
            $errors['identifient_fiscale'] = 'l\'Identifient Fiscale est obligatoire et doit consiste seulement des chiffres';
        }

        $params['ice'] = $this->request->getPost('ice');
        if ((empty($params['ice'])) || (!preg_match('/^[0-9 ]{1,15}$/', $params['ice']))) {
            $errors['ice'] = 'Erreur l\'ICE est obligatoire et doit contient 15 chiffres';
        }

        if (!empty($params['numero_rc'] = $this->request->getPost('numero_rc'))){
            if (!preg_match('/^[A-Z0-9 ]{1,15}$/', $params['numero_rc'])){
                    $errors['numero_rc'] = 'le Numéro de rrgistre du commerce doit contient des lettres en majuscule ou des nombres';
                }
        }
        else {
            $params['numero_rc'] = $this->request->getPost('numero_rc',null,' ');
        }

        
        if (!empty($params['tribunale_immatriculation'] = $this->request->getPost('tribunale_immatriculation'))){
            if (!preg_match('/^[A-Z0-9 ]{2,50}$/', $params['tribunale_immatriculation'])){
                    $errors['tribunale_immatriculation'] = 'le Tribunal d\'immatriculation doit contient des lettres en majuscule ou des nombres';
                }
        }
        else {
            $params['tribunale_immatriculation'] = $this->request->getPost('tribunale_immatriculation',null,' ');
        }

        $params['id_activite'] = $this->request->getPost('id_activite');

       // $params['id_secteur'] = $this->request->getPost('id_secteur');
   
        // $params['is_vip'] = $this->request->getPost('is_vip');
        // if(!preg_match('/^[0-1]{1}$/', $params['is_vip'] )){
        //     $errors['is_vip'] = 'Erreur Saisir 1 pour Sociétaire Vip sinon 0';
        // }

        $params['id_vip'] = $this->request->getPost('id_vip');

        $params['id_categorie_vip'] = $this->request->getPost('id_categorie_vip');

        $params['type_adresse'] = $this->request->getPost('type_adresse');
        if ((empty($params['type_adresse'])) || (!preg_match('/^[R-U]{1}$/', $params['type_adresse']))){
            $errors['type_adresse'] ='taper R pour Rural ou U pour Urbaine';
        }

        if (!empty($params['numero'] = $this->request->getPost('numero'))){
            if (!preg_match('/^[0-9]{2,100}$/', $params['numero'])){
                    $errors['numero'] = 'le numero de votre adresse  doit consiste seulement des nombres';
                }
        }
        else {
            $params['numero'] = $this->request->getPost('numero',null,' ');
        }


       $params['id_voie'] = $this->request->getPost('id_voie');

        $params['complement_adresse'] = $this->request->getPost('complement_adresse');
        if ((empty($params['complement_adresse'])) || (!preg_match('/^[A-Z0-9 ]{2,15}$/', $params['complement_adresse']))) {
            $errors['complement_adresse']  = 'Erreur le complement d\'adresse doit consiste  des lettres en majuscule';
        }

        if (!empty($params['code_postal'] = $this->request->getPost('code_postal'))){
            if (!preg_match('/^[0-9]{2,100}$/', $params['code_postal'])){
                    $errors['code_postal'] = 'Votre Code Postal  doit consiste seulement des nombres';
            }
        }
        else {
            $params['code_postal'] = $this->request->getPost('code_postal',null,' ');
        }
        

        $params['id_ville'] = $this->request->getPost('id_ville');
      
        if (!empty($params['kiyada'] = $this->request->getPost('kiyada'))){
            if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['kiyada'])){
                    $errors['kiyada'] = 'kiyada doit consiste des lettres en majuscule';
            }
        }
        else {
            $params['kiyada'] = $this->request->getPost('kiyada',null,' ');
        }

        if (!empty($params['douar'] = $this->request->getPost('douar'))){
            if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['douar'])){
                    $errors['douar'] = 'Douar doit consiste des lettres en majuscule';
            }
        }
        else {
            $params['douar'] = $this->request->getPost('douar',null,' ');
        }

        // $params['id_province'] = $this->request->getPost('id_province');
        
        // $params['id_commune'] = $this->request->getPost('id_commune');
        
        $params['id_pays_naissance'] = $this->request->getPost('id_pays_naissance');

        if ($errors) {
            $response = new Response();
            $response->setContent(json_encode($errors));
        }
        
        else {
        $response_data = ClientMoraleModel::addClientMorale($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
            }
    return $response;
      
    }

/********************************************************************************************************************/
//      Modifier un clients Morale
//PUT : http://localhost:8000/rsu/ClientMorale/UpdateClientMorale
/********************************************************************************************************************/   
    
public function UpdateClientMoraleAction() {

    $request = new Request();
    $errors=[];
    $params=[];

    $params['raison_sociale'] = $this->request->getPut('raison_sociale');
    if ((empty($params['raison_sociale'])) || (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['raison_sociale']))) {
        $errors['raison_sociale'] = 'la Raison Social est obligatoire et doit consiste des lettres en majuscule';
    }

    $params['nom_abreg'] = $this->request->getPut('nom_abreg');
    if ((empty($params['nom_abreg'])) || (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['nom_abreg']))) {
        $errors['nom_abreg '] = 'Erreur Nom Abrégé doit consiste des lettres en majuscule';
    }

    $params['id_forme_juridique'] = $this->request->getPut('id_forme_juridique');

    $params['tel_fixe'] = $this->request->getPut('tel_fixe');
    if ((empty($params['tel_fixe'])) || (!preg_match('/^[0-9 ]{2,30}$/', $params['tel_fixe']))) {
        $errors['tel_fixe'] = 'Erreur le telephone fixe doit contient seulement des chiffres';
    }

    $params['fax'] = $this->request->getPut('fax');
    if ((empty($params['fax'])) || (!preg_match('/^[0-9 ]{2,30}$/', $params['fax']))) {
        $errors['fax'] = 'Erreur FAX doit contient seulement des chiffres';
    }

    $params['siteweb'] = $this->request->getPut('siteweb',null,' ');

    $params['nom_holding'] = $this->request->getPut('nom_holding');
    if ((empty($params['nom_holding'])) || (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['nom_holding']))){
        $errors['nom_holding'] = 'Erreur Nom du holding doit contient des alphabets en majuscule';
    }

    $params['numero_patente'] = $this->request->getPut('numero_patente');
    if ((empty($params['numero_patente'])) || (!preg_match('/^[0-9 ]{1,15}$/', $params['numero_patente']))) {
        $errors['numero_patente'] = 'Erreur le Numero de patente doit contient seulement des chiffres';
    }
    
    $params['identifient_fiscale'] = $this->request->getPut('identifient_fiscale');
    if((empty($params['identifient_fiscale'])) || (!preg_match('/[0-9 ]{2,100}/', $params['identifient_fiscale']))){
        $errors['identifient_fiscale'] = 'l\'Identifient Fiscale est obligatoire et doit consiste seulement des chiffres';
    }

    $params['ice'] = $this->request->getPut('ice');
    if ((empty($params['ice'])) || (!preg_match('/^[0-9 ]{1,15}$/', $params['ice']))) {
        $errors['ice'] = 'Erreur l\'ICE est obligatoire et doit contient 15 chiffres';
    }

    if (!empty($params['numero_rc'] = $this->request->getPut('numero_rc'))){
        if (!preg_match('/^[A-Z0-9 ]{1,15}$/', $params['numero_rc'])){
                $errors['numero_rc'] = 'le Numéro de rrgistre du commerce doit contient des lettres en majuscule ou des nombres';
            }
    }
    else {
        $params['numero_rc'] = $this->request->getPut('numero_rc',null,' ');
    }

    
    if (!empty($params['tribunale_immatriculation'] = $this->request->getPut('tribunale_immatriculation'))){
        if (!preg_match('/^[A-Z0-9 ]{2,50}$/', $params['tribunale_immatriculation'])){
                $errors['tribunale_immatriculation'] = 'le Tribunal d\'immatriculation doit contient des lettres en majuscule ou des nombres';
            }
    }
    else {
        $params['tribunale_immatriculation'] = $this->request->getPut('tribunale_immatriculation',null,' ');
    }

    $params['id_activite'] = $this->request->getPut('id_activite');

   // $params['id_secteur'] = $this->request->getPut('id_secteur');

    // $params['is_vip'] = $this->request->getPut('is_vip');
    // if(!preg_match('/^[0-1]{1}$/', $params['is_vip'] )){
    //     $errors['is_vip'] = 'Erreur Saisir 1 pour Sociétaire Vip sinon 0';
    // }

    $params['id_vip'] = $this->request->getPut('id_vip');

    $params['id_categorie_vip'] = $this->request->getPut('id_categorie_vip');

    $params['type_adresse'] = $this->request->getPut('type_adresse');
    if ((empty($params['type_adresse'])) || (!preg_match('/^[R-U]{1}$/', $params['type_adresse']))){
        $errors['type_adresse'] ='taper R pour Rural ou U pour Urbaine';
    }

    if (!empty($params['numero'] = $this->request->getPut('numero'))){
        if (!preg_match('/^[0-9]{2,100}$/', $params['numero'])){
                $errors['numero'] = 'le numero de votre adresse  doit consiste seulement des nombres';
            }
    }
    else {
        $params['numero'] = $this->request->getPut('numero',null,' ');
    }

    $params['id_voie'] = $this->request->getPut('id_voie');

    $params['complement_adresse'] = $this->request->getPut('complement_adresse');
    if ((empty($params['complement_adresse'])) || (!preg_match('/^[A-Z0-9 ]{2,15}$/', $params['complement_adresse']))) {
        $errors['complement_adresse']  = 'Erreur le complement d\'adresse doit consiste  des lettres en majuscule';
    }

    if (!empty($params['code_postal'] = $this->request->getPut('code_postal'))){
        if (!preg_match('/^[0-9]{2,100}$/', $params['code_postal'])){
                $errors['code_postal'] = 'Votre Code Postal  doit consiste seulement des nombres';
        }
    }
    else {
        $params['code_postal'] = $this->request->getPut('code_postal',null,' ');
    }
    

    $params['id_ville'] = $this->request->getPut('id_ville');
  
    if (!empty($params['kiyada'] = $this->request->getPut('kiyada'))){
        if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['kiyada'])){
                $errors['kiyada'] = 'kiyada doit consiste des lettres en majuscule';
        }
    }
    else {
        $params['kiyada'] = $this->request->getPut('kiyada',null,' ');
    }

    if (!empty($params['douar'] = $this->request->getPut('douar'))){
        if (!preg_match('/^[A-Z0-9 ]{2,30}$/', $params['douar'])){
                $errors['douar'] = 'Douar doit consiste des lettres en majuscule';
        }
    }
    else {
        $params['douar'] = $this->request->getPut('douar',null,' ');
    }

    // $params['id_province'] = $this->request->getPut('id_province');
    
    // $params['id_commune'] = $this->request->getPut('id_commune');
    
    $params['id_pays_naissance'] = $this->request->getPut('id_pays_naissance');

    $params['id_client_morale'] = $this->dispatcher->getParam('id_client_morale');

    if ($errors) {
        $response = new Response();
        $response->setContent(json_encode($errors));
    }
    
    else {
    $response_data = ClientMoraleModel::updateClientMorale($params);
    $response = new Response();
    $response->setContent(json_encode($response_data));
    }
    return $response;      

}   
/********************************************************************************************************************/
//      Afficher tous les clients Morale
//GET : http://localhost:8000/rsu/ClientMorale/selectAllClientsMorale
/********************************************************************************************************************/   
 
    
    public function selectAllClientsMoraleAction() {

        $response_data = ClientMoraleModel::selectAllClient();
        $result['ClientsMorale'] = $response_data;
        $response = new Response();
        $response->setContent(json_encode($response_data));   
        return $response;

    }

/********************************************************************************************************************/
//       Chercher un client morale par ICE
//Post : http://localhost:8000/rsu/ClientMorale/searchClientMoraleByIce
/********************************************************************************************************************/    

public function searchClientMoraleByIceAction() {
   
    $request = new Request(); 
    $ice = $this->request->getPost('ice');
    $response_data = ClientMoraleModel::searchClientMoraleByIce("'".$ice."'");
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

}    
/********************************************************************************************************************/
//        la fiche sociétaire d'un client morale On utilisons Id_client_morale
//GET : http://localhost:8000/rsu/ClientMorale/getClientMoraleByIdC/id_client_morale/3 (give id_client_physique to get Client)
/********************************************************************************************************************/        


/********************************************************************************************************************/
//       Supprimer client morale On utilisons Id_client_morale
//DELETE : http://localhost:8000/rsu/ClientMorale/deleteClientMoraleById/id_client_morale/3 (give id_client_physique to get Client)
/********************************************************************************************************************/      


}