<?php
use App\Controllers;
use Core\Http\Request;
//use Core\Http\Response;
use Phalcon\Http\Response;
use App\Models;



class PieceIdentitieController extends PrivateController {

/********************************************************************************************************************/
//       Ajouter une nouvelle piece d'identité
//POST : http://localhost:8000/rsu/PieceIdentitie/addnvpieceidentite
/********************************************************************************************************************/    
public function addNvPieceIdentiteAction(){
    
    $request = new Request();
    $errors=[];
    $params=[];

    $params['reference_document'] = $this->request->getPut('reference_document');
    if ((empty($params['reference_document'])) || (!preg_match('/^[A-Z0-9]{2,30}$/', $params['reference_document'])) ) {
        $errors['reference_document'] = 'le reference document est obligatoire saisir des lettres en majuscule et des nombres sans espace';
    }

    $params['date_delivrance'] = $this->request->getPut('date_delivrance');

    $params['lieu_delivrance'] = $this->request->getPut('lieu_delivrance');
        if ((empty($params['lieu_delivrance'])) || (!preg_match('/^[A-Z ]{2,30}$/', $params['lieu_delivrance'])) ) {
            $errors['lieu_delivrance'] = 'le lieu de delivrance est obligatoire saisir des lettres en majuscule';
        }

    $params['date_expiration'] = $this->request->getPut('date_expiration');
    $params['id_client'] = $this->request->getPut('id_client');
    $params['id_type_document'] = $this->request->getPut('id_type_document');

    if ($errors) {
        $response = new Response();
        $response->setContent(json_encode($errors));
    }

    else {
    $response_data = PieceIdentitieModel::ajouterPieceIdentite($params);
    $response = new Response();
    $response->setContent(json_encode($response_data));
    }
    return $response;
}
   
/********************************************************************************************************************/
//   //       Récuperer tous les pieces d'identité d'un client physique
//   //POST : http://localhost:8000/rsu/PieceIdentitie/selectPiecesIdentiteOfClient/uuid_client/db1fab9f-1060-4cf6-8081-a958946ed130 (pour récupérer la reference document CIN)
// /********************************************************************************************************************/       
   
    public function selectPiecesIdentiteOfClientAction(){

        $request = new Request();
        $uuid_client = $this->request->getPost('uuid_client');
        //$response_data = PieceIdentitieModel::selectAllPieceIdentiteOfClientPhysique("'".$uuid_client."'");
        $response_data = PieceIdentitieModel::selectAllPieceIdentiteOfClientPhysique($uuid_client);

        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    }

/********************************************************************************************************************/
//   //       Supprimer une pieces d'identité d'un client physique
//   //DELETE: http://localhost:8000/rsu/PieceIdentitie/deletePieceIdentite/id_piece_identite/ (pour récupérer la reference document CIN)
// /********************************************************************************************************************/          

    public function deletePieceIdentiteAction(){
        
        $params=[];
        $params['id_piece_identite'] = $this->dispatcher->getParam('id_piece_identite');
        $response_data = PieceIdentitieModel::deletePieceOfClientByIdPiece($params);
        $response = new Response();
        $response->setContent(json_encode($response_data));
        return $response;

    }

/********************************************************************************************************************/
//       Récuperer la date de delivrance du permis d'un client
//GET : http://localhost:8000/rsu/PieceIdentitie/selectDatePermis/id_client/4  (pour récupérer la date delivrance)
/********************************************************************************************************************/      
 
    
public function selectDatePermisAction(){

    $id_client = $this->dispatcher->getParam('id_client');
    $response_data = PieceIdentitieModel::selectDatePermis($id_client);
    $response = new Response();
    $response->setContent(json_encode($response_data));
    return $response;

}    
    


}    