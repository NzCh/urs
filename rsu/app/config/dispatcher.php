<?php


use Phalcon\Events\Manager;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Di;

$dispatcher = new Dispatcher();

$eventsManager = new Manager();

//-----------------------------
$di = new Di();
$di->setShared(
    "dispatcher",
    function () use ($di) {
        $eventsManager = new Manager();

        // Attach a listener
        $eventsManager->attach("dispatch:beforeExecuteRoute", $di->get('preflight'));

        $dispatcher = new Dispatcher();
        $dispatcher->setEventsManager($eventsManager);
        $dispatcher->setDefaultNamespace("RealWorld\\Controllers");

        return $dispatcher;
    }
);
//-----------------------------


$eventsManager->attach("dispatch:beforeDispatchLoop", 
        function($event, $dispatcher) 
        {

            $keyParams = array();
            $params = $dispatcher->getParams();

            //Use odd parameters as keys and even as values
            foreach ($params as $number => $value) {
                if ($number & 1) {
                    $keyParams[$params[$number - 1]] = $value;
                }
            }
            //Override parameters
            $dispatcher->setParams($keyParams);
        });

$dispatcher->setEventsManager($eventsManager); 
return $dispatcher;

