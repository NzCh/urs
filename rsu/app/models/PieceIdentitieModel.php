<?php

use Phalcon\Mvc\Model;
use Core\Db\Database;
use App\Controllers;

class PieceIdentitieModel extends Model {

  public static function ajouterPieceIdentite($params){

    $db = new Database();
    $db = new Database();
    $sp = '[dbo].[ps_creer_piece_identite]';
    $result =  $db->execSP($sp,$params);
    return $result;
 
  }
  
  public static function selectDatePermis($id_client){

    $db = new Database();
    $sql = 'SELECT date_delivrance FROM [dbo].[cli_clients] c 
    JOIN [dbo].[cli_piece_identite] p  ON  c.id_client = p.id_client
    JOIN [dbo].[cli_type_document] t ON t.id_type_document = p.id_type_document  AND t.id_type_document = 2
    WHERE c.id_client = ('.$id_client.')';
    $result =  $db->selectAll($sql);
    return $result;

  }


  public static function selectAllPieceIdentiteOfClientPhysique($uuid_client){

    $db = new Database();
    $sql = 'SELECT reference_document,id_piece_identite,p.id_type_document,t.type_document,p.id_client,uuid_client FROM [dbo].[cli_clients] c 
    JOIN [dbo].[cli_piece_identite] p  ON  c.id_client = p.id_client
    JOIN [dbo].[cli_type_document] t ON t.id_type_document = p.id_type_document
    WHERE c.uuid_client = ('.$uuid_client.')';
    $result =  $db->select($sql);
    return $result;

  }

  public static function deletePieceOfClientByIdPiece($params){

    $db = new Database();
    $sp = '[dbo].[ps_delete_piece_identite]';
    $result =  $db->execSP($sp,$params);
    return $result;

  }
}    