<?php

use Phalcon\Mvc\Model;
use Core\Db\Database;
use App\Controllers;

class ClientActiviteModel extends Model {
    
    public static function addActiviteClientPhysique($params) {
        $db = new Database();
        $sp = '[dbo].[ps_nv_activite_client_physique]';
        $result =  $db->execSP($sp,$params);
        return $result;
    }


    public static function deleteActiviteClientPhysique($params) {
        
        $db = new Database();
        //$table = '[dbo].[cli_client_activites]';
        $sp = '[dbo].[ps_delete_activite_cliphysique]';
        $result =  $db->execSP($sp,$params);
        return $result; 

    }


    public static function selectActivitesClientPhysique($id_client) {
        
       $db = new Database();
       $sql = 'SELECT b.id_activite, a.activite,c.id_client,secteur,s.id_secteur,filiere,f.id_filiere,uuid_activite FROM [dbo].[cli_clients] c
       join [dbo].[cli_client_activites] b ON b.id_client = c.id_client  
       join [dbo].[std_activites] a ON a.id_activite = b.id_activite
       join [dbo].[std_filiere] f ON f.id_filiere = a.id_filiere
       join [dbo].[std_secteur] s ON s.id_secteur = f.id_secteur
       where c.id_client = ('.$id_client.')';
       $result =  $db->select($sql);
       return $result; 
        
    }
     
    public static function selectActivitesClientPhysiqueUuid($uuid_client) {
        
       $db = new Database();
       $sql = 'SELECT b.id_activite, a.activite,c.id_client,secteur,s.id_secteur,filiere,f.id_filiere,c.uuid_client,uuid_activite FROM [dbo].[cli_clients] c
       join [dbo].[cli_client_activites] b ON b.id_client = c.id_client  
       join [dbo].[std_activites] a ON a.id_activite = b.id_activite
       join [dbo].[std_filiere] f ON f.id_filiere = a.id_filiere
       join [dbo].[std_secteur] s ON s.id_secteur = f.id_secteur
       where c.uuid_client = ('.$uuid_client.')';
       $result =  $db->select($sql);
       return $result; 
        
    }


    //Activité secteur filiere
    public static function getListeAFSClients(){
      
        $db = new Database();           
        $sql = 'SELECT  f.id_filiere, filiere, s.id_secteur,secteur,a.id_activite, activite,c.id_client,c.nom_client,prenom_client,uuid_activite  from [dbo].[cli_client_activites] x 
        join [dbo].[cli_clients] c on c.id_client = x.id_client
        join [dbo].[cli_client_physique] p on p.id_client = c.id_client
        join [dbo].[std_activites] a on a.id_activite = x.id_activite
        join [dbo].[std_filiere] f on f.id_filiere = a.id_filiere 
        join [dbo].[std_secteur] s on s.id_secteur = f.id_filiere';
        $result = $db->selectAll($sql); 
        return $result;
            
    }







}





