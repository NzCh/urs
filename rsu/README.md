# Projet Conception d'un Référentiel Sociétaire Unique & Assainissement du Portefeuille Sociétaire
## 1 - Contexte et définition du projet:

Le sociétaire est le coeur même des mutuelles dans une assurance.C’est lui qui fait vivre l'activité,ce qui explique l'importance de la gestion de leur critique.le sociétaire a été actuellement crée via plusieurs systèmes d’informations sans aucun identifiant unique et la non-qualité des informations sociétaires(sociétaire dupliqué plusieurs fois,
informations du sociétaire incomplètes ou incorrectes ...) coûte cher pour l'entreprise ce qui est entraine la perte de sociétaires et donc de chiffres d’affaires.

## 2 - Objectifs du projet:

Ce projet vise à consolider et enrichir la base de données « Personne », il a pour objectif :
  1. La mise en place d'un référentiel sociétaire unique et partagé entre tous les applicatifs métiers (IARD,
  Indemnisation, Vie, Santé et Bancassurance, ...).
  2. L'assainissement du portefeuille sociétaire existant (la correction, l'enrichissement et la
  déduplication des données).

## 3 - Acteur:

| Acteur        | Cas d'utilisation                                                                    | 
|-------------- |--------------------------------------------------------------------------------------| 
| Administrateur|Gerer les personnes physique                                                          |
|               |(ajouter un noveau client physique ,modifier supprimer et consulter la fiche          |
|               |signalitique du sociétaire)                                                           |               
|               |Gerer les personnes morales                                                           |  
|               |(ajouter un noveau client physique ,modifier supprimer et consulter la fiche d'une    |
|               |personnes morale)                                                                     |
|               |Gestions des tables de nomenclatures(villes,profession,pays...)                       |
|               | Gestion des pieces d'identitité(carte national,passport ...)                         |
|               | Gestion des activité d'un sociétaire                                                 |  

## 4 - USE CASE:

![Cas d’utilisation géneral](screenshot/usecase.png)

## 5 - Diagramme de classes :

![Diagramme de classes](screenshot/diagrame-de-classes.png)


## 6 Outils du dévellopement coté Backend/Frontend

la plateforme est dévellopé par le framework Phalcon 4.0  pour le backend et VueJs pour le frontend.
le port du lancement coté backend : http://localhost:8000/
le port du lancement sur VueJs    : http://localhost:3000/

## 7 Service de la gestion des personnes physiques

Pour ajouter un noveau personne physique : 
**POST : http://localhost:8000/rsu/ClientPhysique/addClientPhysique**
![Ajouter un noveau personne physique](screenshot/ajouterclientphysique.png)
![](screenshot/ajouterclientphysique2.png)
**GET  : http://localhost:8000/rsu/ClientPhysique/getClientPhysiqueByIdClientPhysique/id_client_physique/5**
![Consulter le portefeuille d'une personne physique](screenshot/fiche-signalitique-client-physique.png)
